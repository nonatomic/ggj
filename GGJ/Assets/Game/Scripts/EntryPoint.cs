﻿using UnityEngine;
using Nonatomic.Utomic;
using System.Collections;

namespace GGJ {

	public class EntryPoint : MonoBehaviour {

		private void Awake() {

			this.AddUtomic();

			// Features			
			this.AddFeature<PreloaderFeature>();
			this.AddFeature<SetupFeature>();
			this.AddFeature<StartFeature>();
            this.AddFeature<GameFeature>();
            this.AddFeature<GameUIFeature>();
            this.AddFeature<GameOverFeature>();

            //Play some tunes
            this.Inject<PlaySoundSignal>().Dispatch("spooky2", 1f, 1f);

		}
	}
}
