﻿using UnityEngine;
using System.Collections;

public class UpdateTimerComponent : MonoBehaviour {

	private Material material;
	public float val;

	public void Awake(){
		material = this.GetComponent<Renderer>().material;
	}

	public void SetTime(float val){
		this.val = val;
		material.SetFloat("_CutOff", val);
	}

	public void SetColor(Color32 color){
		material.SetColor("_Color", color);
	}
}
