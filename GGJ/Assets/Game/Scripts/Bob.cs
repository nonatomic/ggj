﻿using UnityEngine;
using System.Collections;

public class Bob : MonoBehaviour {

	// Use this for initialization
	void Start () {
		Tween();
	}

	void Tween() {
		LeanTween.moveY(gameObject, 0.08f, 1.8f).setEase(LeanTweenType.easeInOutBack).setLoopPingPong();
	}
}
