﻿using UnityEngine;
using System.Collections;
using Nonatomic.Utomic;

public class ChestComponent : MonoBehaviour {
	public GameObject lid;
    public bool open = false;
    public bool opening = false;
    public bool closing = false;

	public void OnTriggerEnter(Collider col){
		Debug.Log("CHEST ENTER:" + col.gameObject.name);
        
        var objName = col.gameObject.name;
        if(objName != "Player" && objName != "LivingPlayer" )
            return;
        
        if(!open && !opening){
           opening = true;
           OpenChest();
        }
	}
    
    public void OnTriggerExit(Collider col){
       
    //    var objName = col.gameObject.name;
    //     if(objName != "Player" && objName != "LivingPlayer" )
    //         return;
            
       Debug.Log("CHEST EXIT:" + col.gameObject.name);
        if(open && !closing){
            closing = true;
            CloseChest();
        }
    }
    
    public void OpenChest(){
         //close chest
         this.Log("OPENING");
          lid.transform.eulerAngles = new Vector3(90f,180f,180f);
            // LeanTween.rotate(lid, new Vector3(10f, 0f, 180f), 0.5f).setOnComplete(()=>{
            //     open = true;
            //     closing = false;
            // });
    }
    
    public void CloseChest(){
        //open chest
        this.Log("CLOSING");
             lid.transform.eulerAngles = new Vector3(84.89922f,180f,180f);
            // LeanTween.rotate(lid,new Vector3(0,0,180f), 0.3f).setOnComplete(()=>{
            //    open = false;
            //    opening = false;
            // });
    }
}
