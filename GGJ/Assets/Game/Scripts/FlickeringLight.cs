﻿using UnityEngine;
using Nonatomic.Utomic;

namespace GGJ {

    [RequireComponent(typeof(Light))]
    public class FlickeringLight : MonoBehaviour {

        public float minIntensity = 0.25f;
        public float maxIntensity = 0.5f;
        private float random;
        private Light light;

        public void Start(){
            random = Random.Range(0f, 6f);
            light = this.GetComponent<Light>();
        }

        public void Update(){
            float noise = Mathf.PerlinNoise(random, Time.time);
            light.intensity = Mathf.Lerp(minIntensity, maxIntensity, noise);
        }
    }
}

