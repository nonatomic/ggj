

//
//   _  _  ___  _  _   _ _____ ___  __  __ ___ ___   _   _	  _
//  | \| |/ _ \| \| | /_\_   _/ _ \|  \/  |_ _/ __| | | | |_ __| |
//  | .` | (_) | .` |/ _ \| || (_) | |\/| || | (__  | |_|  _/ _` |_
//  |_|\_|\___/|_|\_/_/ \_\_| \___/|_|  |_|___\___| |____\__\__,_(_)
//
//  Created, owned and maintained by Nonatomic Ltd.
//  https://www.nonatomic.co.uk
//  support@nonatomic.co.uk
//
//  Copyright 2015 - All Rights Reserved
//
using UnityEngine;
using Nonatomic.Utomic;

namespace GGJ {

	public class RoomConfig {

		public int x;
		public int y;
		public int rowMatch;
		public int columnMatch;
		public Vector2 gridPos;
		public bool legal;
		public GameObject room;
		public bool start;
		public bool end;

		//neighbouring elements
		public RoomConfig north;
		public RoomConfig northEast;
		public RoomConfig east;
		public RoomConfig southEast;
		public RoomConfig south;
		public RoomConfig southWest;
		public RoomConfig west;
		public RoomConfig northWest;


		public RoomConfig(int x, int y){

			this.x = x;
			this.y = y;
			this.gridPos = new 
			Vector2(x,y);
		}

		/**
		* tells you if you the provided element is a neighbouring on the hor, vert
		*/
		public bool IsNeighbour(RoomConfig element){

			this.Log("My Neighbour: " + x + "," + y + " = n:" + north + ", e:" + east + ", s:" + south + ", w:" + west, Color.cyan);
			this.Log("Your Neighbour: " + x + "," + y + " = n:" + element.north + ", e:" + element.east + ", s:" + element.south + ", w:" + element.west, Color.cyan);
			if(north == element || east == element || south == element || west == element)
				return true;

			return false;
		}

		/**
		* Find the element directly opposite this cell from the originElement
		*/
		public RoomConfig OppositeElement(RoomConfig originElement){

			if(originElement == north)
				return south;

			if(originElement == east)
				return west;

			if(originElement == south)
				return north;

			if(originElement == west)
				return east;

			return null;
		}

		public void SeverLink(RoomConfig room){

			if(room == north){

				north.south = null;
				north = null;
			}
			else if(room == south){
				south.north = null;
				south = null;
			}
			else if(room == east){
				east.west = null;
				east = null;
			}
			else if(room == room.west){
				west.east = null;
				west = null;
			}
		}

		public void Query(IRoomGridQuery query){
			query.Accept(this);
		}

		public void Visit(IRoomGridQuery query){

			if(north != null)
				query.Accept(north);

			if(east != null)
				query.Accept(east);

			if(south != null)
				query.Accept(south);

			if(west != null)
				query.Accept(west);

		}

	}
}

