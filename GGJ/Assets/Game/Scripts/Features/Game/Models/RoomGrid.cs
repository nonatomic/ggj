//
//   _  _  ___  _  _   _ _____ ___  __  __ ___ ___   _   _	  _
//  | \| |/ _ \| \| | /_\_   _/ _ \|  \/  |_ _/ __| | | | |_ __| |
//  | .` | (_) | .` |/ _ \| || (_) | |\/| || | (__  | |_|  _/ _` |_
//  |_|\_|\___/|_|\_/_/ \_\_| \___/|_|  |_|___\___| |____\__\__,_(_)
//
//  Created, owned and maintained by Nonatomic Ltd.
//  https://www.nonatomic.co.uk
//  support@nonatomic.co.uk
//
//  Copyright 2015 - All Rights Reserved
//

using UnityEngine;
using System.Collections.Generic;
using System.Linq;
using System;
using Nonatomic.Utomic;

namespace GGJ {

	public class RoomGrid : IRoomGrid {

		private List<RoomConfig> elements;

		private int width;
		public int Width{ get{ return width; } }

		private int height;
		public int Height{ get{ return height; } }

		public RoomGrid Setup(int width, int height) {

			this.width = width;
			this.height = height;
			this.elements = new List<RoomConfig>();

			//Create elements
			for(int w = 0; w < Width; w++) {
				for(int h = 0; h < Height; h++) {
					elements.Add(new RoomConfig(w,h));
				}
			}

			//Set element neighbours
			ResetNeighbours();

			return this;
		}

		public void ResetNeighbours() {

			//Set element neighbours
			for(int w = 0; w < Width; w++) {
				for(int h = 0; h < Height; h++){
					var cell 		= Cell(w+0, h+0);
					cell.north 		= Cell(w+0, h-1);
					cell.northEast 	= Cell(w+1, h-1);
					cell.east 		= Cell(w+1, h+0);
					cell.southEast 	= Cell(w+1, h+1);
					cell.south		= Cell(w+0, h+1);
					cell.southWest 	= Cell(w-1, h+1);
					cell.west 		= Cell(w-1, h+0);
					cell.northWest 	= Cell(w-1, h-1);
				}
			}
		}

		public List<RoomConfig> Row(int row) {
			return elements.FindAll(element => element.y == row).ToList();
		}

		public List<RoomConfig> Column(int col) {
			return elements.FindAll(element => element.x == col).ToList();
		}

		public List<RoomConfig> All(){
			return elements.ToList();
		}

		public RoomConfig Cell(Vector2 cell) {
			return elements.Find(element => element.gridPos == cell);
		}

		public RoomConfig Cell(int x, int y) {
			return elements.Find(element => element.x == x && element.y == y);
		}

		/**
		* Returns an area using cell as the center. Size relates to the
		* number of cells from the center. Size 0 will return just the cell
		* in the center. Size 1 will return a 3x3 grid with cell at the center
		*/
		public List<RoomConfig> Area(Vector2 cell, int size) {
			return elements.FindAll(element => Mathf.Abs(element.x - cell.x) <= size &&
												 Mathf.Abs(element.y - cell.y) <= size).ToList();
		}

		/**
		* Returns an area marked by two cells
		*/
		public List<RoomConfig> Area(Vector2 cellFrom, Vector2 cellTo) {
			return elements.FindAll(element => element.x >= cellFrom.x && element.x <= cellTo.x &&
											   element.y >= cellFrom.y && element.y <= cellTo.y).ToList();
		}

		public List<RoomConfig> FindAll(Predicate<RoomConfig> search) {
			return elements.FindAll(search);
		}

		public RoomConfig Find(Predicate<RoomConfig> search) {
			return elements.Find(search);
		}

		private static string ColorToHex(Color32 color) {
			return "#" + color.r.ToString("X2") + color.g.ToString("X2") + color.b.ToString("X2");
		}
	}
}
