//
//   _  _  ___  _  _   _ _____ ___  __  __ ___ ___   _   _	  _
//  | \| |/ _ \| \| | /_\_   _/ _ \|  \/  |_ _/ __| | | | |_ __| |
//  | .` | (_) | .` |/ _ \| || (_) | |\/| || | (__  | |_|  _/ _` |_
//  |_|\_|\___/|_|\_/_/ \_\_| \___/|_|  |_|___\___| |____\__\__,_(_)
//
//  Created, owned and maintained by Nonatomic Ltd.
//  https://www.nonatomic.co.uk
//  support@nonatomic.co.uk
//
//  Copyright 2015 - All Rights Reserved
//

using UnityEngine;
using Nonatomic.Utomic;
using System.Collections.Generic;

namespace GGJ {

	public interface ILevelModel {

		IRoomGrid RoomGrid{ get; set; }
		RoomConfig CurrentRoom{ get; set; }
		List<RoomConfig> PreviousRooms{ get; set; }
		Vector2 LevelSize{ get; }
		List<RoomConfig> ValidPath{ get; set; }
		List<GameObject> OtherRooms{ get; set;}
	}
}
