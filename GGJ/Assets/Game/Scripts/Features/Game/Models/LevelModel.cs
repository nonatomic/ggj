//
//   _  _  ___  _  _   _ _____ ___  __  __ ___ ___   _   _	  _
//  | \| |/ _ \| \| | /_\_   _/ _ \|  \/  |_ _/ __| | | | |_ __| |
//  | .` | (_) | .` |/ _ \| || (_) | |\/| || | (__  | |_|  _/ _` |_
//  |_|\_|\___/|_|\_/_/ \_\_| \___/|_|  |_|___\___| |____\__\__,_(_)
//
//  Created, owned and maintained by Nonatomic Ltd.
//  https://www.nonatomic.co.uk
//  support@nonatomic.co.uk
//
//  Copyright 2015 - All Rights Reserved
//

using UnityEngine;
using Nonatomic.Utomic;
using System.Collections.Generic;

namespace GGJ {

	public class LevelModel : ILevelModel {

		public IRoomGrid RoomGrid{ get; set; }
		public RoomConfig CurrentRoom{ get; set; } 
		public List<RoomConfig> PreviousRooms{ get; set; }
		public Vector2 LevelSize{ get{ return new Vector2(10,10); } }
		public List<RoomConfig> ValidPath{ get; set;}
		public List<GameObject> OtherRooms {get; set;}
	}
}
