//
//   _  _  ___  _  _   _ _____ ___  __  __ ___ ___   _   _	  _
//  | \| |/ _ \| \| | /_\_   _/ _ \|  \/  |_ _/ __| | | | |_ __| |
//  | .` | (_) | .` |/ _ \| || (_) | |\/| || | (__  | |_|  _/ _` |_
//  |_|\_|\___/|_|\_/_/ \_\_| \___/|_|  |_|___\___| |____\__\__,_(_)
//
//  Created, owned and maintained by Nonatomic Ltd.
//  https://www.nonatomic.co.uk
//  support@nonatomic.co.uk
//
//  Copyright 2015 - All Rights Reserved
//

using UnityEngine;
using System.Collections.Generic;
using System.Linq;
using System;
using Nonatomic.Utomic;

namespace GGJ {

	public interface IRoomGrid {

		int Width{ get; }
		int Height{ get; }
		RoomGrid Setup(int width, int height);
		void ResetNeighbours();
		List<RoomConfig> Row(int row);
		List<RoomConfig> Column(int col) ;
		List<RoomConfig> All();
		RoomConfig Cell(Vector2 cell);
		RoomConfig Cell(int x, int y);
		List<RoomConfig> Area(Vector2 cell, int size);
		List<RoomConfig> Area(Vector2 cellFrom, Vector2 cellTo);
		List<RoomConfig> FindAll(Predicate<RoomConfig> search);
		RoomConfig Find(Predicate<RoomConfig> search);
	}
}
