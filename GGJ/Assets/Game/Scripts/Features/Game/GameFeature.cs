//
//   _  _  ___  _  _   _ _____ ___  __  __ ___ ___   _   _	  _
//  | \| |/ _ \| \| | /_\_   _/ _ \|  \/  |_ _/ __| | | | |_ __| |
//  | .` | (_) | .` |/ _ \| || (_) | |\/| || | (__  | |_|  _/ _` |_
//  |_|\_|\___/|_|\_/_/ \_\_| \___/|_|  |_|___\___| |____\__\__,_(_)
//
//  Created, owned and maintained by Nonatomic Ltd.
//  https://www.nonatomic.co.uk
//  support@nonatomic.co.uk
//
//  Copyright 2015 - All Rights Reserved
//

using UnityEngine;
using Nonatomic.Utomic;

namespace GGJ {

	public class GameFeature : IFeature{

		public void Wire() {
            
            //Signal => Command
            this.CommandMap().Bind<SetupGameSignal, SetupGameCommand>();
            this.CommandMap().Bind<DestroyGameSignal, DestroyGameCommand>();
            this.CommandMap().Bind<BuildRoomDataSignal, BuildRoomDataCommand>();
            this.CommandMap().Bind<ChangeRoomSignal, ChangeRoomCommand>();
            this.CommandMap().Bind<EnterLivingModeSignal, EnterLivingModeCommand>();
            this.CommandMap().Bind<EnterDeadModeSignal, EnterDeadModeCommand>();
            this.CommandMap().Bind<PermaDeathSignal, PermaDeathCommand>();
            this.CommandMap().Bind<AbandonHostSignal, AbandonHostCommand>();
            this.CommandMap().Bind<PlayFootStepsSignal, PlayFootStepsCommand>();


            //Injector Bindings
            this.Bind<IRoomFactory, RoomFactory>();
            this.Bind<ILevelModel, LevelModel>();
		}

		public void Structure(){
			
		}

		public void Setup(){


		}

		public void Run(){
            
		}
	}
}
