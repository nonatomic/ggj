//
//   _  _  ___  _  _   _ _____ ___  __  __ ___ ___   _   _	  _
//  | \| |/ _ \| \| | /_\_   _/ _ \|  \/  |_ _/ __| | | | |_ __| |
//  | .` | (_) | .` |/ _ \| || (_) | |\/| || | (__  | |_|  _/ _` |_
//  |_|\_|\___/|_|\_/_/ \_\_| \___/|_|  |_|___\___| |____\__\__,_(_)
//
//  Created, owned and maintained by Nonatomic Ltd.
//  https://www.nonatomic.co.uk
//  support@nonatomic.co.uk
//
//  Copyright 2015 - All Rights Reserved
//
using UnityEngine;
using Nonatomic.Utomic;
using System.Collections.Generic;

namespace GGJ {

	public class ValidPathToEndZoneQuery : IRoomGridQuery {

		public bool result;

		private List<RoomConfig> list = new List<RoomConfig>();
		private IRoomGrid grid;
		private Vector2 origin;

		public void Run(IRoomGrid grid, Vector2 origin){

			this.grid = grid;
			this.origin = origin;
			this.list.Clear();

			this.grid.Cell(origin).Query(this);
		}

		public void Accept(RoomConfig element){

			if(list.Count == 0){
				list.Add(element);
			}
			
			var allNull = ((element.north == null || list.Contains(element.north)) && 
						   (element.east == null || list.Contains(element.east)) && 
						   (element.south == null || list.Contains(element.south)) && 
						   (element.west == null || list.Contains(element.west)));

			if(allNull){
				this.Log("FAIL:" + list.Count);
				return;
			}

			//pick a random non-null direction
			// var rnd = Random.Range(0,3);
			RoomConfig[] doors = new RoomConfig[4]{element.north, element.east, element.south, element.west};
			var nxt = doors[Random.Range(0,4)];

			if(nxt != null && !list.Contains(nxt)){
				list.Add(nxt);

				//look for random length path between 6-8
				var length = Random.Range(10,20);
				if(list.Count == length){
					this.Log("SUCCESS");
					result = true;
				}
				else{
					Accept(nxt);
				}
			}
			else{
				Accept(element);
			}

		}

		public List<RoomConfig> GetResult(){
			return list;
		}

	}
}
