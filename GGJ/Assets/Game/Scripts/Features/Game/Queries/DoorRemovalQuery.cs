//
//   _  _  ___  _  _   _ _____ ___  __  __ ___ ___   _   _	  _
//  | \| |/ _ \| \| | /_\_   _/ _ \|  \/  |_ _/ __| | | | |_ __| |
//  | .` | (_) | .` |/ _ \| || (_) | |\/| || | (__  | |_|  _/ _` |_
//  |_|\_|\___/|_|\_/_/ \_\_| \___/|_|  |_|___\___| |____\__\__,_(_)
//
//  Created, owned and maintained by Nonatomic Ltd.
//  https://www.nonatomic.co.uk
//  support@nonatomic.co.uk
//
//  Copyright 2015 - All Rights Reserved
//
using UnityEngine;
using Nonatomic.Utomic;
using System.Collections.Generic;
using System.Collections;
using System.Linq;

namespace GGJ {

	class Portal{
		public RoomConfig roomFrom;
		public RoomConfig roomTo;

		public Portal(RoomConfig roomFrom, RoomConfig roomTo){
			this.roomFrom = roomFrom;
			this.roomTo = roomTo;
		}
	}

	public class DoorRemovalQuery : IRoomGridQuery {

		private List<RoomConfig> validRooms;
		private List<RoomConfig> visited = new List<RoomConfig>();
		private List<Portal> results = new List<Portal>();
		private bool east = true;

		public DoorRemovalQuery(List<RoomConfig> rooms){
			this.validRooms = rooms;
		}

		public void Accept(RoomConfig element){
			
			//record
			visited.Add(element);

			//collect
			CheckValidDoor(element, element.north);
			CheckValidDoor(element, element.east);
			CheckValidDoor(element, element.south);
			CheckValidDoor(element, element.west);


			//traverse
			if(east && element.east != null){
				element.east.Query(this);
			}
			else if(!east && element.west != null){
				element.west.Query(this);
			}
			else if(element.south != null){
				east = !east;
				element.south.Query(this);
			}
			else{
				Complete();
			}

			
			

		}

		private void CheckValidDoor(RoomConfig roomFrom, RoomConfig roomTo){

			if(roomFrom == null || roomTo == null)
				return;

			if(!validRooms.Contains(roomFrom) && !validRooms.Contains(roomTo)){
				results.Add(new Portal(roomFrom, roomTo));
			}
		}

		private void Complete(){
			
			//remove some frickin doors
			var doorRemovalPerc = 0.4f;
			var doorCount = (int)(results.Count * doorRemovalPerc);
			var shuffled = results.Shuffle();
			this.Log("DOOR COUNT:" + doorCount);
			for(int i = 0; i < doorCount; i++){
				RemoveDoor(shuffled[i]);
			}


			this.Log("COMPLETE:" + visited.Count + ", " + results.Count);

		}

		private void RemoveDoor(Portal portal) {
			portal.roomFrom.SeverLink(portal.roomTo);
			
		}


	}
}
