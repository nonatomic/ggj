//
//   _  _  ___  _  _   _ _____ ___  __  __ ___ ___   _   _	  _
//  | \| |/ _ \| \| | /_\_   _/ _ \|  \/  |_ _/ __| | | | |_ __| |
//  | .` | (_) | .` |/ _ \| || (_) | |\/| || | (__  | |_|  _/ _` |_
//  |_|\_|\___/|_|\_/_/ \_\_| \___/|_|  |_|___\___| |____\__\__,_(_)
//
//  Created, owned and maintained by Nonatomic Ltd.
//  https://www.nonatomic.co.uk
//  support@nonatomic.co.uk
//
//  Copyright 2015 - All Rights Reserved
//
using UnityEngine;
using Nonatomic.Utomic;
using System.Collections.Generic;

namespace GGJ {

	public class ChangeRoomCommand : ICommand {

		public void Execute(params object[] parameters) {

			var roomFrom = (RoomConfig) parameters[0];
			var roomTo = (RoomConfig) parameters[1];
			var doorPosition = (DoorPosition) parameters[2];
			var parent = roomFrom.room.transform.parent;
			var goFactory = this.Inject<IGameObjectFactory>();

			//Create the room we enter
			var roomFactory = this.Inject<IRoomFactory>();
			roomFactory.Make(roomTo, parent.gameObject, Vector3.zero);

			//Move the player to the entrance door
			var player = this.EntityList().GetComponent<PlayerComponent>();

			//use to debug who travels through the doors
			// player.gameObject.GetComponentInChildren<Renderer>().material.color = Color.red;
			
			player.gameObject.transform.SetParent(roomTo.room.transform, false);

			switch(doorPosition){
				case DoorPosition.North:
					player.transform.localPosition = new Vector3(0f, 0f, -0.4f);
				break;
				case DoorPosition.East:
					player.transform.localPosition = new Vector3(-0.4f,0f,0f);
				break;
				case DoorPosition.South:
					player.transform.localPosition = new Vector3(0f, 0f, 0.4f);
				break;
				case DoorPosition.West:
					player.transform.localPosition = new Vector3(0.4f, 0f, 0f);
				break;
			}

			//Update mini map
			this.Inject<SetMiniMapRoomSignal>().Dispatch(roomTo);
			this.Log("CHANGE ROOMS");

			//return the room to the pool
			var levelModel = this.Inject<ILevelModel>();
			roomFrom.room.ReturnToPool();

			// foreach(GameObject room in levelModel.OtherRooms){
			// 	if(room != null)
			// 		room.ReturnToPool();
			// }
			// levelModel.OtherRooms.Clear();

			//store the current room
			this.Inject<ILevelModel>().CurrentRoom = roomTo;

			//try other sides3
			// var eastPref = roomFactory.Make(roomTo.east, parent.gameObject, new Vector3(0.976f, 0f, 0f));
			// var westPref = roomFactory.Make(roomTo.west, parent.gameObject, new Vector3(-0.976f, 0f, 0f));
			// var northPref = roomFactory.Make(roomTo.north, parent.gameObject, new Vector3(0f, 0f, 0.976f));
			// var southPref = roomFactory.Make(roomTo.south, parent.gameObject, new Vector3(0f, 0f, -0.976f));
			// levelModel.OtherRooms = new List<GameObject>(){
			// 	eastPref, westPref, northPref, southPref
			// };
		}
	}
}
