//
//   _  _  ___  _  _   _ _____ ___  __  __ ___ ___   _   _	  _
//  | \| |/ _ \| \| | /_\_   _/ _ \|  \/  |_ _/ __| | | | |_ __| |
//  | .` | (_) | .` |/ _ \| || (_) | |\/| || | (__  | |_|  _/ _` |_
//  |_|\_|\___/|_|\_/_/ \_\_| \___/|_|  |_|___\___| |____\__\__,_(_)
//
//  Created, owned and maintained by Nonatomic Ltd.
//  https://www.nonatomic.co.uk
//  support@nonatomic.co.uk
//
//  Copyright 2015 - All Rights Reserved
//
using UnityEngine;
using Nonatomic.Utomic;
using UnityStandardAssets.ImageEffects;

namespace GGJ {

	public class EnterLivingModeCommand : ICommand {

		public void Execute(params object[] parameters) {

			//get the 3D camera
			var camera3D = this.EntityList().GetComponent<Camera>(e => e.GetComponent<Camera3DComponent>() != null);
			camera3D.GetComponent<Grayscale>().enabled = false;
			camera3D.GetComponent<BloomOptimized>().enabled = false;
			camera3D.GetComponent<GlobalFog>().enabled = false;
			camera3D.GetComponent<MotionBlur>().enabled = false;
            this.EntityList().GetEntity<MainLightComponent>().SetActive(false);

		}
	}
}
