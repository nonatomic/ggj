//
//   _  _  ___  _  _   _ _____ ___  __  __ ___ ___   _   _	  _
//  | \| |/ _ \| \| | /_\_   _/ _ \|  \/  |_ _/ __| | | | |_ __| |
//  | .` | (_) | .` |/ _ \| || (_) | |\/| || | (__  | |_|  _/ _` |_
//  |_|\_|\___/|_|\_/_/ \_\_| \___/|_|  |_|___\___| |____\__\__,_(_)
//
//  Created, owned and maintained by Nonatomic Ltd.
//  https://www.nonatomic.co.uk
//  support@nonatomic.co.uk
//
//  Copyright 2015 - All Rights Reserved
//
using UnityEngine;
using Nonatomic.Utomic;
using UnityStandardAssets.ImageEffects;
using UnityStandardAssets.Characters.ThirdPerson;

namespace GGJ {

	public class AbandonHostCommand : ICommand {

		public void Execute(params object[] parameters) {

			//if currently a ghost return
			var playerComponent = this.EntityList().GetComponent<PlayerComponent>();
			if(playerComponent.gameObject.GetComponent<DeadPlayerComponent>() != null){
				return;
			}


			//get the 3D camera
			var camera3D = this.EntityList().GetComponent<Camera>(e => e.GetComponent<Camera3DComponent>() != null);
			camera3D.GetComponent<Grayscale>().enabled = true;
			camera3D.GetComponent<BloomOptimized>().enabled = true;
			camera3D.GetComponent<GlobalFog>().enabled = true;
			camera3D.GetComponent<MotionBlur>().enabled = true;
			
			//make the players ghost emerge from the living player

			//do the opposite of all this shit
			var deadPlayer = this.EntityList().GetEntity<DeadPlayerComponent>();

			//disable the control scripts
			var currentPlayerControls = playerComponent.gameObject.GetComponent<ThirdPersonUserControl>();
			var currentPlayerChar = playerComponent.gameObject.GetComponent<ThirdPersonCharacter>();

			//remove all motion
			playerComponent.gameObject.GetComponent<Animator>().SetFloat("Forward", 0f);
			playerComponent.gameObject.GetComponent<Rigidbody>().velocity = Vector3.zero;

			currentPlayerControls.enabled = false;
			currentPlayerChar.enabled = false;

			//destroy the old player comp
			this.EntityList().RemoveComponent(playerComponent, true);
			this.Log("REMOVE PLAYER COMP 1", Color.red );

			//Add a Death Timer - Limit time allowed as a spirit before you die
			deadPlayer.GetComponent<DeadTimerComponent>().Activate();
			deadPlayer.AddEntityComponent<PlayerComponent>();
			this.Log("ADD PLAYER COMP 1", Color.red );

			//Show the dead player
			deadPlayer.SetActive(true);
			var room = this.Inject<ILevelModel>().CurrentRoom;
			deadPlayer.transform.SetParent(room.room.transform, false);
			deadPlayer.transform.localPosition = Vector3.zero;

			//enable control on new character
			var hostPlayerControls = deadPlayer.GetComponent<ThirdPersonUserControl>();
			var hostPlayerChar = deadPlayer.GetComponent<ThirdPersonCharacter>();
			hostPlayerControls.enabled = true;
			hostPlayerChar.enabled = true;

			//Make living colliders all triggers
			var livingPlayers = this.EntityList().GetEntities<LivingPlayerComponent>();
			foreach(GameObject livingPlayer in livingPlayers){
				var collider = livingPlayer.GetComponent<Collider>();
				collider.isTrigger = true;

				livingPlayer.GetComponent<Rigidbody>().constraints = RigidbodyConstraints.FreezePositionX | RigidbodyConstraints.FreezePositionY | RigidbodyConstraints.FreezePositionZ | RigidbodyConstraints.FreezeRotationX | RigidbodyConstraints.FreezeRotationY | RigidbodyConstraints.FreezeRotationZ;
			}

			//Complete the living timers because we may abandon ship early
			this.EntityList().GetComponent<LivingTimerComponent>(e => e == playerComponent.gameObject).Complete(false);
		}
	}
}
