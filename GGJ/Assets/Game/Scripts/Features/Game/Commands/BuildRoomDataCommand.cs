//
//   _  _  ___  _  _   _ _____ ___  __  __ ___ ___   _   _	  _
//  | \| |/ _ \| \| | /_\_   _/ _ \|  \/  |_ _/ __| | | | |_ __| |
//  | .` | (_) | .` |/ _ \| || (_) | |\/| || | (__  | |_|  _/ _` |_
//  |_|\_|\___/|_|\_/_/ \_\_| \___/|_|  |_|___\___| |____\__\__,_(_)
//
//  Created, owned and maintained by Nonatomic Ltd.
//  https://www.nonatomic.co.uk
//  support@nonatomic.co.uk
//
//  Copyright 2015 - All Rights Reserved
//
using UnityEngine;
using Nonatomic.Utomic;

namespace GGJ {

	public class BuildRoomDataCommand : ICommand {

		public void Execute(params object[] parameters) {

			//build some room data
			//start by building a grid of interconnecting rooms
			var levelModel = this.Inject<ILevelModel>();
			var levelSize = levelModel.LevelSize;
			var grid = new RoomGrid();
			grid.Setup((int)levelSize.x, (int)levelSize.y);

			this.Inject<ILevelModel>().RoomGrid = grid;

		}
	}
}
