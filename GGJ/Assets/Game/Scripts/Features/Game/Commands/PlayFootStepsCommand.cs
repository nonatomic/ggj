//
//   _  _  ___  _  _   _ _____ ___  __  __ ___ ___   _   _	  _
//  | \| |/ _ \| \| | /_\_   _/ _ \|  \/  |_ _/ __| | | | |_ __| |
//  | .` | (_) | .` |/ _ \| || (_) | |\/| || | (__  | |_|  _/ _` |_
//  |_|\_|\___/|_|\_/_/ \_\_| \___/|_|  |_|___\___| |____\__\__,_(_)
//
//  Created, owned and maintained by Nonatomic Ltd.
//  https://www.nonatomic.co.uk
//  support@nonatomic.co.uk
//
//  Copyright 2015 - All Rights Reserved
//
using UnityEngine;
using Nonatomic.Utomic;

namespace GGJ {

	public class PlayFootStepsCommand : ICommand {

		private float stepRate = 0.5f;
		private float floatRate = 0.9f;
		private float nextFootStep = 0.5f;
		private float nextGhost = 3f;

		public void Execute(params object[] parameters) {

			var player = this.EntityList().GetComponent<PlayerComponent>();

			if(player.GetComponent<DeadPlayerComponent>() != null){
				//play ghost sound
				if(Time.time > nextGhost){
					nextGhost = Time.time + floatRate;
					float pitch = Random.Range(10, 30)/10f;
					var rnd = Random.Range(1,5);
					this.Inject<PlaySoundSignal>().Dispatch("ghost" + rnd.ToString(),0.1f,pitch);
				}
			}
			else{
				//play footsteps
				if(Time.time > nextFootStep){
					nextFootStep = Time.time + stepRate;
					float pitch = Random.Range(10, 30)/10f;
					this.Inject<PlaySoundSignal>().Dispatch("step",0.3f,pitch);
				}
			}


			
		}
	}
}
