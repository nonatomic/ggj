//
//   _  _  ___  _  _   _ _____ ___  __  __ ___ ___   _   _	  _
//  | \| |/ _ \| \| | /_\_   _/ _ \|  \/  |_ _/ __| | | | |_ __| |
//  | .` | (_) | .` |/ _ \| || (_) | |\/| || | (__  | |_|  _/ _` |_
//  |_|\_|\___/|_|\_/_/ \_\_| \___/|_|  |_|___\___| |____\__\__,_(_)
//
//  Created, owned and maintained by Nonatomic Ltd.
//  https://www.nonatomic.co.uk
//  support@nonatomic.co.uk
//
//  Copyright 2015 - All Rights Reserved
//
using UnityEngine;
using Nonatomic.Utomic;
using System.Collections.Generic;

namespace GGJ {

	public class SetupGameCommand : ICommand {

		public void Execute(params object[] parameters) {

			//show background
			var prefabFactory = this.Inject<IPrefabFactory>();
			var goFactory = this.Inject<IGameObjectFactory>();
			var roomFactory = this.Inject<IRoomFactory>();
			var levelModel = this.Inject<ILevelModel>();

			var parent = this.EntityList().GetEntity<UtomicCore>();
			var game = goFactory.Make<GameComponent>("Game", parent).gameObject;
			var background = prefabFactory.Make("Background", game);
			prefabFactory.Make<MainLightComponent>("MainLight", game);

			//Keys
			var keyMap = game.AddEntityComponent<KeyMap>();
			keyMap.Add(KeyCode.Space, KeyState.Down, this.Inject<AbandonHostSignal>());
			keyMap.Add(new KeyCode[8]{KeyCode.W, KeyCode.A, KeyCode.S, KeyCode.D, KeyCode.LeftArrow, KeyCode.RightArrow, KeyCode.DownArrow, KeyCode.UpArrow}, KeyState.Down, this.Inject<PlayFootStepsSignal>());
			keyMap.Add(new KeyCode[8]{KeyCode.W, KeyCode.A, KeyCode.S, KeyCode.D, KeyCode.LeftArrow, KeyCode.RightArrow, KeyCode.DownArrow, KeyCode.UpArrow}, KeyState.HeldDown, this.Inject<PlayFootStepsSignal>());
	

			//Build a grid of rooms
			this.Inject<BuildRoomDataSignal>().Dispatch();

			//Build the first room - Test with room 0,0
			var startingPos = new Vector2(Random.Range(0, levelModel.LevelSize.x),
										  Random.Range(0, levelModel.LevelSize.y));

			var room = levelModel.RoomGrid.Cell((int)startingPos.x, (int)startingPos.y);
			levelModel.CurrentRoom = room;
			

			//Deploy the ValidPathQuery to find an endzone
			var path = new ValidPathToEndZoneQuery();
			levelModel.CurrentRoom.Query(path);
			var validPath = this.Inject<ILevelModel>().ValidPath = path.GetResult();

			//set start & end
			validPath[validPath.Count-1].end = true;
			validPath[0].start = true;
			this.Log("START END:" + validPath[0].x + "," + validPath[0].y + "," + levelModel.CurrentRoom.x + "," + levelModel.CurrentRoom.y);

			//Remove valid doors
			var doorRemoval = new DoorRemovalQuery(validPath);
			levelModel.RoomGrid.Cell(0,0).Query(doorRemoval);


			var roomPref = roomFactory.Make(room, game, Vector3.zero);

			//try other sides3
			// var eastPref = roomFactory.Make(room.east, game, new Vector3(0.976f, 0f, 0f));
			// var westPref = roomFactory.Make(room.west, game, new Vector3(-0.976f, 0f, 0f));
			// var northPref = roomFactory.Make(room.north, game, new Vector3(0f, 0f, 0.976f));
			// var southPref = roomFactory.Make(room.south, game, new Vector3(0f, 0f, -0.976f));
			// levelModel.OtherRooms = new List<GameObject>(){
			// 	eastPref, westPref, northPref, southPref
			// };


			//Build a player
			var player = prefabFactory.Make<PlayerComponent>("Player", roomPref);
			player.gameObject.AddEntityComponent<HostComponent>();
			player.gameObject.AddEntityComponent<DeadPlayerComponent>();
			player.gameObject.transform.localPosition = new Vector3(0,0,0);
			player.gameObject.transform.eulerAngles = new Vector3(0,90,0);


			//Add a Death Timer
			player.gameObject.AddEntityComponent<DeadTimerComponent>().Activate();

			//Build the hosts
			var hostA = prefabFactory.Make<LivingPlayerComponent>("LivingPlayer", roomPref);
			hostA.gameObject.AddEntityComponent<HostComponent>();
			hostA.gameObject.AddEntityComponent<BadHostTimerComponent>();

			var hostB = prefabFactory.Make<LivingPlayerComponent>("LivingPlayer", roomPref);
			hostB.gameObject.AddEntityComponent<HostComponent>();
			hostB.gameObject.AddEntityComponent<BadHostTimerComponent>();

			var hostC = prefabFactory.Make<LivingPlayerComponent>("LivingPlayer", roomPref);
			hostC.gameObject.AddEntityComponent<HostComponent>();
			hostC.gameObject.AddEntityComponent<BadHostTimerComponent>();

			//Position around in a circle
			hostA.transform.localPosition = new Vector3(0.097f, 0.0173f, -0.285f);
			hostA.transform.eulerAngles = new Vector3(0,347,0);

			hostB.transform.localPosition = new Vector3(0.2165f, 0.0173f, 0.2252f);
			hostB.transform.eulerAngles = new Vector3(0f,222.4f,0f);

			hostC.transform.localPosition = new Vector3(-0.286f, 0.0173f, 0.0097f);
			hostC.transform.eulerAngles = new Vector3(0.1180f,0.1180f,0f);

			//show game ui
			this.Inject<ChangeMenuStateSignal>().Dispatch(new GameState());



		}
	}
}
