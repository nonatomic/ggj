//
//   _  _  ___  _  _   _ _____ ___  __  __ ___ ___   _   _	  _
//  | \| |/ _ \| \| | /_\_   _/ _ \|  \/  |_ _/ __| | | | |_ __| |
//  | .` | (_) | .` |/ _ \| || (_) | |\/| || | (__  | |_|  _/ _` |_
//  |_|\_|\___/|_|\_/_/ \_\_| \___/|_|  |_|___\___| |____\__\__,_(_)
//
//  Created, owned and maintained by Nonatomic Ltd.
//  https://www.nonatomic.co.uk
//  support@nonatomic.co.uk
//
//  Copyright 2016 - All Rights Reserved
//

using Nonatomic.Utomic;
using UnityEngine;
using System.Collections.Generic;


namespace GGJ {

	public class RoomFactory : IRoomFactory {

		private IPrefabFactory prefabFactory;
		private IGameObjectFactory goFactory;
		private IEntityPool pool;
		private List<string> roomTypes = new List<string>(){
			"RoomTemplateA",
			"RoomTemplateB",
			"RoomTemplateC"
		};

		public RoomFactory(){
			prefabFactory = this.Inject<IPrefabFactory>();
			goFactory = this.Inject<IGameObjectFactory>();
			pool = this.Inject<IEntityPool>();
		}

		public GameObject Make(RoomConfig room, GameObject parent, Vector3 pos) {

			if(room == null)
				return null;
				
			var pooledRoom = pool.Pop<RoomComponent>(r => r.x == room.x && r.y == room.y);

			if(pooledRoom == null){
				var roomComp = goFactory.Make<RoomComponent>("Room-" + room.x + "," + room.y, parent);
				roomComp.x = room.x;
				roomComp.y = room.y;
				pooledRoom = roomComp.gameObject;
				prefabFactory.Make("Floor", pooledRoom);
                
                MakePentagram(room, pooledRoom);
			    MakeFurniture(room, pooledRoom);
			}

			pooledRoom.transform.localPosition = pos;

			MakeWall(pooledRoom, 0, room, DoorPosition.North);
			MakeWall(pooledRoom, 90, room, DoorPosition.East);
			MakeWall(pooledRoom, 180, room, DoorPosition.South);
			MakeWall(pooledRoom, -90, room, DoorPosition.West);


			

			//provide a reference to the room GameObject on the RoomConfig
			room.room = pooledRoom;

			return pooledRoom;
		}

		private void MakeFurniture(RoomConfig room, GameObject parent){

			var roomType = roomTypes[Random.Range(0,roomTypes.Count)];
			var furniture = prefabFactory.Make(roomType, parent);
			var direction = Random.Range(0,5);
			furniture.transform.eulerAngles = new Vector3(0,direction*90,0);
		}


		private void MakePentagram(RoomConfig room, GameObject parent){
			this.Log("PENT:" + room.start +"," + room.end);
			if(!room.start && !room.end)
				return;

			prefabFactory.Make("Pentogram", parent);
		}

		private void MakeWall(GameObject go, int rotation, RoomConfig roomFrom, DoorPosition doorPosition){

			RoomConfig roomTo = null;

			//get the destination of the room
			switch(doorPosition){
				case DoorPosition.North:
					roomTo = roomFrom.north;
				break;
				case DoorPosition.East:
					roomTo = roomFrom.east;
				break;
				case DoorPosition.South:
					roomTo = roomFrom.south;
				break;
				case DoorPosition.West:
					roomTo = roomFrom.west;
				break;	
			}

			if(roomTo == null){
				this.Log("NULL WALL");
			}

			//Make a wall
			var wallType = roomTo == null ? "Wall" : "WallWithDoor";
			var wallPref = prefabFactory.Make(wallType,go);

			//Make a door
			if(roomTo != null){
				var doorWay = prefabFactory.Make<DoorWayComponent>("DoorWay",wallPref);
				doorWay.roomFrom = roomFrom;
				doorWay.roomTo = roomTo;
				doorWay.doorPosition = doorPosition;
			}
			
			//rotate for the 4 axis
			wallPref.transform.eulerAngles = new Vector3(0, rotation, 0);
		}
	}
}
