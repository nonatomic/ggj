using UnityEngine;
using Nonatomic.Utomic;
using UnityStandardAssets.Characters.ThirdPerson;

namespace GGJ {

	public class LivingTimerComponent : MonoBehaviour {

		private float duration = 20.0f;
		private float remainingTime;
		private bool active;
		private UpdateTimerComponent timeRenderer;
		public float val;

		public void Awake(){
			remainingTime = duration;
			timeRenderer = this.GetComponentInChildren<UpdateTimerComponent>();
			val = remainingTime;
			if(timeRenderer != null)
				timeRenderer.SetTime(1f);
		}

		public void Activate(){
			// timeRenderer.SetColor(Color.green);
			remainingTime = duration;
			active= true;
		}

		public void Update(){

			if(!active)
				return;

			remainingTime -= Time.deltaTime;
			val = remainingTime;

			if(timeRenderer != null)
				timeRenderer.SetTime(1-(remainingTime/duration));

			if(remainingTime <= 0){

				Complete(true);
			}
		}

		public void Complete(bool enterDeadMode){
			if(timeRenderer != null)
					timeRenderer.SetTime(1);

				var badHost = gameObject.GetComponent<BadHostTimerComponent>();
				if(badHost != null)
					badHost.Activate();

				this.Inject<EnterDeadModeSignal>().Dispatch();
				active = false;
		}
	}
}
