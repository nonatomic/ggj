using UnityEngine;
using Nonatomic.Utomic;
using UnityStandardAssets.Characters.ThirdPerson;

namespace GGJ {

	public class LivingPlayerComponent : MonoBehaviour {

		private ThirdPersonUserControl thirdPersonControl;

		public void Awake(){

			//turn of the thirdperson controller
			thirdPersonControl = gameObject.GetComponent<ThirdPersonUserControl>();
			thirdPersonControl.enabled = false;
		}
	}
}
