using UnityEngine;
using Nonatomic.Utomic;
using UnityStandardAssets.Characters.ThirdPerson;

namespace GGJ {

	public class DeadTimerComponent : MonoBehaviour {
		
		private float duration = 20.0f;
		private float remainingTime;
		private bool active;
		private UpdateTimerComponent timeRenderer;
		public float val;

		public void Awake(){
			remainingTime = duration;
			timeRenderer = this.GetComponentInChildren<UpdateTimerComponent>();
			val = remainingTime;
			if(timeRenderer != null)
				timeRenderer.SetTime(1f);
		}

		public void Activate(){
			// timeRenderer.SetColor(Color.cyan);
			remainingTime = duration;
			active= true;
		}

		public void Update(){

			if(!active)
				return;

			remainingTime -= Time.deltaTime;
			val = remainingTime;

			if(timeRenderer != null)
				timeRenderer.SetTime(1-(remainingTime/duration));

			if(remainingTime <= 0){
				if(timeRenderer != null)
					timeRenderer.SetTime(1);
					
				this.Inject<PermaDeathSignal>().Dispatch();
				active = false;
			}
		}
	}
}
