using UnityEngine;
using Nonatomic.Utomic;
using UnityStandardAssets.Characters.ThirdPerson;

namespace GGJ {

	public class BadHostTimerComponent : MonoBehaviour {

		private bool coolingDown;
		public bool CoolingDown{ get{ return coolingDown; }}

		private float duration = 20.0f;
		private float remainingTime;
		private bool active;
		private UpdateTimerComponent timeRenderer;
		public float val;

		public void Awake(){
			remainingTime = duration;
			timeRenderer = this.GetComponent<UpdateTimerComponent>();
			val = remainingTime;
			if(timeRenderer != null)
				timeRenderer.SetTime(1f);
		}

		public void Activate(){
			// timeRenderer.SetColor(Color.red);
			coolingDown = true;
			remainingTime = duration;
			active= true;
		}

		public void Update(){

			if(!active)
				return;

			remainingTime -= Time.deltaTime;
			val = remainingTime;

			if(timeRenderer != null){
				timeRenderer.SetTime(1-(remainingTime/duration));
			}

			if(remainingTime <= 0){
				if(timeRenderer != null)
					timeRenderer.SetTime(1);
					
				coolingDown = false;
				active = false;
			}
		}
	}
}
