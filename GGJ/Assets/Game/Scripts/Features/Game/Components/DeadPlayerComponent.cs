using UnityEngine;
using Nonatomic.Utomic;
using UnityStandardAssets.Characters.ThirdPerson;

namespace GGJ {

	public class DeadPlayerComponent : MonoBehaviour {

		private ThirdPersonUserControl thirdPersonControl;

		public void Awake(){

			//turn of the thirdperson controller
			thirdPersonControl = gameObject.GetComponent<ThirdPersonUserControl>();
			thirdPersonControl.enabled = true;
		}
	}
}
