using UnityEngine;
using Nonatomic.Utomic;
using UnityStandardAssets.Characters.ThirdPerson;

namespace GGJ {

	public class HostComponent : MonoBehaviour {

		public void OnTriggerEnter(Collider col){
			this.Log("HIT:" + col.gameObject.name, Color.cyan);

			//Down allow living peeps to get possessed when cooling down from a previous possession
			var badHost = col.gameObject.GetComponent<BadHostTimerComponent>();
			if(badHost != null && badHost.CoolingDown)
				return;

			//alow the character to be possesed
			var host = col.gameObject.GetComponent<HostComponent>();
			if(host != null){

				//remove the player control from the current player
				var player = gameObject.GetComponent<PlayerComponent>();

				
				this.EntityList().RemoveComponent(player);

				//this fixes an issue with multiple PlayComponents floating around
				//I think there maybe an issue with the destory method within the EntityList
				//but thats an issue for another time.....
				MonoBehaviour.Destroy(player);

				//disable the control scripts
				var currentPlayerControls = gameObject.GetComponent<ThirdPersonUserControl>();
				var currentPlayerChar = gameObject.GetComponent<ThirdPersonCharacter>();

				//remove all motion
				gameObject.GetComponent<Animator>().SetFloat("Forward", 0f);
				gameObject.GetComponent<Rigidbody>().velocity = Vector3.zero;

				currentPlayerControls.enabled = false;
				currentPlayerChar.enabled = false;

				//hide current player if not living
				var dead = gameObject.GetComponent<DeadPlayerComponent>();
				if(dead != null){
					dead.gameObject.SetActive(false);
					this.Inject<EnterLivingModeSignal>().Dispatch();
				}

				//Add the player component to the new player
				col.gameObject.AddEntityComponent<PlayerComponent>();


				//enable control on new character
				var hostPlayerControls = col.gameObject.GetComponent<ThirdPersonUserControl>();
				var hostPlayerChar = col.gameObject.GetComponent<ThirdPersonCharacter>();
				hostPlayerControls.enabled = true;
				hostPlayerChar.enabled = true;

				//reenable the colliders of the living when playing as a living person
				var livingPlayers = this.EntityList().GetEntities<LivingPlayerComponent>();
				foreach(GameObject livingPlayer in livingPlayers){
					var collider = livingPlayer.GetComponent<Collider>();
					collider.isTrigger = false;

					livingPlayer.GetComponent<Rigidbody>().constraints = RigidbodyConstraints.FreezePositionX | RigidbodyConstraints.FreezePositionY | RigidbodyConstraints.FreezePositionZ | RigidbodyConstraints.FreezeRotationX | RigidbodyConstraints.FreezeRotationY | RigidbodyConstraints.FreezeRotationZ;
				}

				col.gameObject.GetComponent<Rigidbody>().constraints = RigidbodyConstraints.FreezePositionY | RigidbodyConstraints.FreezeRotationX | RigidbodyConstraints.FreezeRotationZ;
			
				//start a timer
				var timer = col.gameObject.AddEntityComponent<LivingTimerComponent>();
				timer.Activate();
			}
		}
	}
}

