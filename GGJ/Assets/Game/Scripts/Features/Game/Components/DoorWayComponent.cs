using UnityEngine;
using Nonatomic.Utomic;

namespace GGJ {

	public enum DoorPosition{
		North,
		East,
		South,
		West
	}

	public class DoorWayComponent : MonoBehaviour {

		public RoomConfig roomTo;
		public RoomConfig roomFrom;
		public DoorPosition doorPosition;

		private void OnTriggerEnter(){
			
			//remove the room
			this.Inject<ChangeRoomSignal>().Dispatch(roomFrom, roomTo, doorPosition);

		}

	}
}
