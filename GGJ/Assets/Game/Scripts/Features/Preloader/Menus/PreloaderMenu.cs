//
//   _  _  ___  _  _   _ _____ ___  __  __ ___ ___   _   _	  _
//  | \| |/ _ \| \| | /_\_   _/ _ \|  \/  |_ _/ __| | | | |_ __| |
//  | .` | (_) | .` |/ _ \| || (_) | |\/| || | (__  | |_|  _/ _` |_
//  |_|\_|\___/|_|\_/_/ \_\_| \___/|_|  |_|___\___| |____\__\__,_(_)
//
//  Created, owned and maintained by Nonatomic Ltd.
//  https://www.nonatomic.co.uk
//  support@nonatomic.co.uk
//
//  Copyright 2015 - All Rights Reserved
//

using UnityEngine;
using Nonatomic.Utomic;

namespace GGJ {

	public class PreloaderMenu : IMenu {

		private GameObject container;

		public PreloaderMenu(){

			var uiFactory = this.Inject<IUIFactory>();
            var prefabFactory = this.Inject<IPrefabFactory>();
			var canvas = this.EntityList().GetEntity<UICanvasComponent>();
			container = uiFactory.MakeContainer("PreloadMenu", canvas);
            
			prefabFactory.Make<HorizontalPreloadBarComponent>("PreloaderBar", container);
			
			container.SetActive(false);

		}

		public void Show(MenuState state) {

			container.SetActive(true);

		}

		public void Hide(MenuState state) {
			container.SetActive(false);

		}
	}
}
