//
//   _  _  ___  _  _   _ _____ ___  __  __ ___ ___   _   _	  _
//  | \| |/ _ \| \| | /_\_   _/ _ \|  \/  |_ _/ __| | | | |_ __| |
//  | .` | (_) | .` |/ _ \| || (_) | |\/| || | (__  | |_|  _/ _` |_
//  |_|\_|\___/|_|\_/_/ \_\_| \___/|_|  |_|___\___| |____\__\__,_(_)
//
//  Created, owned and maintained by Nonatomic Ltd.
//  https://www.nonatomic.co.uk
//  support@nonatomic.co.uk
//
//  Copyright 2015 - All Rights Reserved
//

using UnityEngine;
using UnityEngine.UI;
using Nonatomic.Utomic;

namespace GGJ {

	[RequireComponent (typeof (Image))]
	public class HorizontalPreloadBarComponent : MonoBehaviour {

		private Image img;

		public void Awake(){
			img = this.GetComponent<Image>();
			img.type = Image.Type.Filled;
			img.fillMethod = Image.FillMethod.Horizontal;
		}

		public void SetValue(float percLoaded){
			img.fillAmount = percLoaded;
		}
	}
}
