using Nonatomic.Utomic;
using UnityEngine;

namespace GGJ {

	/**
	* All features have been ran
	*/
	public class PreloadFeatureCompleteCommand : ICommand {

		public void Execute(params object[] parameters){

			//show game state
			this.Inject<ChangeMenuStateSignal>().Dispatch(new StartState());
		}
	}
}
