//
//   _  _  ___  _  _   _ _____ ___  __  __ ___ ___   _   _	  _
//  | \| |/ _ \| \| | /_\_   _/ _ \|  \/  |_ _/ __| | | | |_ __| |
//  | .` | (_) | .` |/ _ \| || (_) | |\/| || | (__  | |_|  _/ _` |_
//  |_|\_|\___/|_|\_/_/ \_\_| \___/|_|  |_|___\___| |____\__\__,_(_)
//
//  Created, owned and maintained by Nonatomic Ltd.
//  https://www.nonatomic.co.uk
//  support@nonatomic.co.uk
//
//  Copyright 2015 - All Rights Reserved
//

using UnityEngine;
using UnityEngine.UI;
using Nonatomic.Utomic;

namespace GGJ {

	public class GameOverMenu : IMenu {

		private GameObject container;
		private IPrefabFactory prefabFactory;
		private IUIFactory uiFactory;
		private GameObject panel;

		public GameOverMenu(){

			prefabFactory = this.Inject<IPrefabFactory>();
			uiFactory = this.Inject<IUIFactory>();

			var canvas = this.EntityList().GetEntity<UICanvasComponent>();
			container = uiFactory.MakeContainer("GameOverMenu", canvas);

			/**
			* Example of building menus via either prefab or code
			* Code option is more flexible, but by prefab is quicker
			* for rapid prototyping.
			*
			* When building prefabs I recommend only using builtin Unity scripts
			* on the prefab.
			*/
			MakeMenuUsingPrefabs();

			container.SetActive(false);

		}

		private void MakeMenuUsingPrefabs(){

			panel = prefabFactory.Make("GameOverPanel", container);
			prefabFactory.MakeButton("ReStartButton", panel, this.Inject<ReStartButtonSignal>());
		}

		public void Show(MenuState state) {

			container.SetActive(true);

			var t = panel.gameObject.RectTransform();
			t.SetPosition(0,-1000);
			LeanTween.moveY(t, 0, 0.5f).setEase(LeanTweenType.easeInBack);

		}

		public void Hide(MenuState state) {
			container.SetActive(false);

			LeanTween.moveY(panel.gameObject.RectTransform(), -1000, 0.5f).setEase(LeanTweenType.easeOutBack);
		}
	}
}
