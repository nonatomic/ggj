//
//   _  _  ___  _  _   _ _____ ___  __  __ ___ ___   _   _	  _
//  | \| |/ _ \| \| | /_\_   _/ _ \|  \/  |_ _/ __| | | | |_ __| |
//  | .` | (_) | .` |/ _ \| || (_) | |\/| || | (__  | |_|  _/ _` |_
//  |_|\_|\___/|_|\_/_/ \_\_| \___/|_|  |_|___\___| |____\__\__,_(_)
//
//  Created, owned and maintained by Nonatomic Ltd.
//  https://www.nonatomic.co.uk
//  support@nonatomic.co.uk
//
//  Copyright 2015 - All Rights Reserved
//

using UnityEngine;
using UnityEngine.UI;
using Nonatomic.Utomic;

namespace GGJ {

	public class GameUI : IMenu {

		private GameObject container;
		// private IPrefabFactory prefabFactory;
		private IUIFactory uiFactory;
		private GameObject panel;
		private bool builtMiniMap;
		private GameObject miniMap;

		public GameUI(){

			// prefabFactory = this.Inject<IPrefabFactory>();
			uiFactory = this.Inject<IUIFactory>();

			var canvas = this.EntityList().GetEntity<UICanvasComponent>();
			container = uiFactory.MakeContainer("GameUI", canvas);
			container.SetActive(false);

		}

		public void Reset(){
			builtMiniMap = false;
		}

		public void BuildMiniMap() {

			if(builtMiniMap)
				return;
			builtMiniMap = true;

			//mini map
			var levelModel = this.Inject<ILevelModel>();
			var grid = levelModel.RoomGrid;
			var currentRoom = levelModel.CurrentRoom;
			miniMap = uiFactory.MakeContainer("MiniMap", container);
			var tileSize = new Vector2(10,10);
			var tileSpacing = new Vector2(tileSize.x + 2f, tileSize.y + 2f);

			if(grid == null)
				return;

			miniMap.RectTransform().SetAnchorMinMax(1,1).SetOffsetMinMax(0,0).SetPosition(-(grid.Width * tileSpacing.x), -(grid.Height * tileSpacing.y));

			//temp
			// var validPath = levelModel.ValidPath;

			for(int w = 0; w < grid.Width; w++){
				for(int h = 0; h < grid.Height; h++){
					//create a tile in our minimap
					var miniRoom = uiFactory.MakeImage<MiniRoomComponent>("", miniMap); //hoping if I send an empty image path it'll render a blank coloured square
					miniRoom.gridRef.x = w;
					miniRoom.gridRef.y = (grid.Height-h-1);
					miniRoom.name = "MiniRoom";
					miniRoom.gameObject.RectTransform().SetAnchorCenter().SetSize(tileSize.x, tileSize.y).SetPosition(w*tileSpacing.x, h*tileSpacing.y);

					//show doors
					var room = grid.Cell((int)miniRoom.gridRef.x, (int)miniRoom.gridRef.y);
					BuildDoor(room.north, 0f, 6f, miniRoom);
					BuildDoor(room.east, 6f, 0f, miniRoom);
					BuildDoor(room.south, 0f, -6f, miniRoom);
					BuildDoor(room.west, -6f, 0f, miniRoom);

					//render validpath
					// if(validPath != null){
					// 	var index = validPath.FindIndex(r => r.x == miniRoom.gridRef.x && r.y == miniRoom.gridRef.y);
						
					// 	if(index > -1){
					// 		miniRoom.Highlight(Color.Lerp(Color.white, Color.black, index * 0.03f));
					// 	}
					// }
				}
			}

			this.Inject<SetMiniMapRoomSignal>().Dispatch(currentRoom);
		}

		public void BuildDoor(RoomConfig room, float posX, float posY, MiniRoomComponent miniRoom){

			if(room == null)
				return;


			var door = uiFactory.MakeImage("", miniRoom.gameObject);
			door.gameObject.RectTransform().SetAnchorCenter().SetSize(2,2).SetPosition(posX, posY);
			miniRoom.doors.Add(door);
		}

		public void Show(MenuState state) {
			container.SetActive(true);
			BuildMiniMap();
		}
        
		public void Hide(MenuState state) {
			container.SetActive(false);
			MonoBehaviour.Destroy(miniMap);
			builtMiniMap = false;
		}
	}
}
