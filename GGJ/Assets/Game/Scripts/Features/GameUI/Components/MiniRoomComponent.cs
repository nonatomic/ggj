using UnityEngine;
using Nonatomic.Utomic;
using UnityEngine.UI;
using System.Collections.Generic;

namespace GGJ {

	public class MiniRoomComponent : MonoBehaviour {

		public static Color32 Normal = new Color32(101, 98, 88, 255);
		public static Color32 Selected = new Color32(234, 183, 17, 255);
		public static Color32 Adjacent = new Color32(153, 132, 62, 255);
		public static Color32 SubAdjacent = new Color32(131, 116, 67, 255);
		public List<Image> doors = new List<Image>();

		public Vector2 gridRef = Vector2.zero;
		private Image image;

		public bool IsHighlighted(){
			return image.color != Normal;
		}

		public void Awake(){
			image = gameObject.GetComponent<Image>();
			image.color = Normal;

		}

		public void Start(){

			foreach(Image door in doors){
				door.color = Normal;
			}
		}

		public void Highlight(Color32 color){
			image.color = color;

			foreach(Image door in doors){
				door.color = color;
			}
		}

		public void Reset(){
			image.color = Normal;

			foreach(Image door in doors){
				door.color = Normal;
			}
		}
	}
}
