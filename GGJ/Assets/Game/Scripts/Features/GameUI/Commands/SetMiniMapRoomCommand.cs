//
//   _  _  ___  _  _   _ _____ ___  __  __ ___ ___   _   _	  _
//  | \| |/ _ \| \| | /_\_   _/ _ \|  \/  |_ _/ __| | | | |_ __| |
//  | .` | (_) | .` |/ _ \| || (_) | |\/| || | (__  | |_|  _/ _` |_
//  |_|\_|\___/|_|\_/_/ \_\_| \___/|_|  |_|___\___| |____\__\__,_(_)
//
//  Created, owned and maintained by Nonatomic Ltd.
//  https://www.nonatomic.co.uk
//  support@nonatomic.co.uk
//
//  Copyright 2015 - All Rights Reserved
//
using UnityEngine;
using Nonatomic.Utomic;
using System.Collections.Generic;
using System.Linq;

namespace GGJ {

	public class SetMiniMapRoomCommand : ICommand {

		public void Execute(params object[] parameters) {

			var levelModel = this.Inject<ILevelModel>();
			levelModel.PreviousRooms = levelModel.PreviousRooms ?? new List<RoomConfig>();

			//current room
			var room = (RoomConfig) parameters[0];

			//remove previous entries
			levelModel.PreviousRooms.RemoveAll(r => r == room);
			levelModel.PreviousRooms.Add(room);
			
			float step = 0f;
			for(var i = levelModel.PreviousRooms.Count-1; i >= 0; i--){
				RoomConfig previousRoom = levelModel.PreviousRooms[i];
				Highlight(previousRoom, MiniRoomComponent.Selected, MiniRoomComponent.Normal, step);
				step+=0.1f;
				if(step > 1)
					break;
			}

			//cap the limit of the list at 20
			var cap = 20;
			if(levelModel.PreviousRooms.Count >= cap){
				 levelModel.PreviousRooms.RemoveAt(0);
			}	
		}

		private void Highlight(RoomConfig room, Color32 colorFrom, Color32 colorTo, float step){
			if(room != null){
				var mini = this.EntityList().GetComponent<MiniRoomComponent>(r => r.gridRef.x == room.x && r.gridRef.y == room.y);
				if(mini != null){
					mini.Highlight(Color.Lerp(colorFrom, colorTo, step));
				}
			}
		}
	}
}
