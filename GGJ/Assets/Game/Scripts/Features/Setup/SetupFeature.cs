//
//   _  _  ___  _  _   _ _____ ___  __  __ ___ ___   _   _	  _
//  | \| |/ _ \| \| | /_\_   _/ _ \|  \/  |_ _/ __| | | | |_ __| |
//  | .` | (_) | .` |/ _ \| || (_) | |\/| || | (__  | |_|  _/ _` |_
//  |_|\_|\___/|_|\_/_/ \_\_| \___/|_|  |_|___\___| |____\__\__,_(_)
//
//  Created, owned and maintained by Nonatomic Ltd.
//  https://www.nonatomic.co.uk
//  support@nonatomic.co.uk
//
//  Copyright 2015 - All Rights Reserved
//

using UnityEngine;
using Nonatomic.Utomic;
using UnityStandardAssets.ImageEffects;

namespace GGJ {

	public class SetupFeature : IFeature{

		public void Wire() {

		}

		public void Structure(){
			//To gain access to the root GameObject fetch the UtomicCore Entity
			var parent = this.EntityList().GetEntity<UtomicCore>();
			var cameraFactory = this.Inject<ICameraFactory>();

			//The 2D Camera will render both GUI and Sprites
			cameraFactory.Make<Camera2DComponent>(new Camera2D(), parent);

			//The UI Canvas is a container for all UI
			var canvas = this.Inject<ICanvasFactory>().Make<UICanvasComponent>(new UICanvas(), parent);

			//The Sprite Canvas is a container for all Sprites
			var spriteCanvas = new GameObject("SpriteCanvas");
			spriteCanvas.transform.SetParent(parent.transform, false);
			spriteCanvas.AddEntityComponent<SpriteCanvasComponent>();
			this.EntityList().AddEntity(spriteCanvas);

			//3D camera will render our rooms
			var camera = cameraFactory.Make<Camera3DComponent>(new Camera3D(), parent);
			
			var bloom = camera.gameObject.AddEntityComponent<BloomOptimized>();
			bloom.fastBloomShader = Resources.Load<Shader>("MobileBloom");
			bloom.intensity = 1.28f;

			var gray = camera.gameObject.AddEntityComponent<Grayscale>();
			gray.shader = Resources.Load<Shader>("GrayscaleEffect");

			var fog = camera.gameObject.AddEntityComponent<GlobalFog>();
			fog.fogShader = Resources.Load<Shader>("GlobalFog");
			fog.height = 12.69f;
			fog.heightDensity = 4.92f;
            
            var motionBlur = camera.gameObject.AddEntityComponent<MotionBlur>();
            motionBlur.shader = Resources.Load<Shader>("MotionBlur");


			var prefabFactory = this.Inject<IPrefabFactory>();
			var vignette = prefabFactory.Make("vignette", canvas.gameObject);
			vignette.RectTransform().SetAnchorMinMax(0f,0f,1f,1f);

		}

		public void Setup(){


		}

		public void Run(){

		}
	}
}
