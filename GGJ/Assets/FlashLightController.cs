﻿using UnityEngine;
using System.Collections;

namespace GGJ{
    public class FlashLightController : MonoBehaviour {

        public GameObject flashLight;

        // Use this for initialization
        void Start () {
            
            flashLight.SetActive(false);
        }

        // Update is called once per frame
        void Update () {
            
            if(this.GetComponent<PlayerComponent>() != null){
                flashLight.SetActive(true);
            }
            else{
                flashLight.SetActive(false);
            }
        }
    }   
}

